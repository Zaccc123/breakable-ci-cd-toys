from django.shortcuts import render
from tasksapp.models import Task
from tasksapp.forms import MessageForm
from django.http import HttpResponseRedirect
import paho.mqtt.publish as publish
from tasksapp import mqtt


# Create your views here.
def home(request):
    tasks = Task.objects.all()
    context = {'tasks': tasks, }

    return render(request, 'tasksapp/home.html', context)


def channel(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = MessageForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            print("sending")
            publish.single("tasksapp/channel", form.cleaned_data['message'], hostname="192.168.1.34")
            return HttpResponseRedirect('/channel/')
    else:
        form = MessageForm()

    return render(request, 'tasksapp/channel.html', {'form': form})
