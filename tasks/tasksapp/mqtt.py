import paho.mqtt.client as mqtt
from tasksapp.models import Task


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("tasksapp/channel")
    # client.subscribe("$SYS/#")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("New Message: " + msg.topic + " " + str(msg.payload) + str(msg.qos))
    Task.objects.create(title=msg.payload)


def on_subscribe(client, userdata, mid, granted_qos):
    print("subscribed to tasksapp/channel")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe

client.connect_async("192.168.1.34", 1883, 60)
# client.connect("192.168.1.15", 1883, 60)
# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_start()
# client.loop(timeout=1.0, max_packets=1)
