from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from tasksapp.models import Task


# Create your tests here.
class HomeViewTests(TestCase):
    def setUp(self):
        superuser = User.objects.create_superuser('test', 'test@api.com', 'testpassword')
        self.user = superuser
        self.client.login(username=superuser.username, password='testpassword')

    def test_get_all_tasks_at_home(self):
        """
        Ensure that home view display all tasks
        """
        task_one = Task.objects.create(title="This is first task")
        task_two = Task.objects.create(title="This is second task")
        response = self.client.get(reverse('home'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['tasks'][0], task_one)
        self.assertEqual(response.context['tasks'][1], task_two)
