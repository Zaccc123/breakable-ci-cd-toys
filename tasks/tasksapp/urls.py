from django.conf.urls import url
from tasksapp import views

urlpatterns = [
    url(r'^(?i)channel/$', views.channel, name='channel'),
    url(r'^', views.home, name='home'),
]
