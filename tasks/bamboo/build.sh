#!/bin/bash
# install requirements for pip for bambooci 

echo "Running build.sh at $bamboo_working_directory"
source $bamboo_working_directory/tasks/bin/activate
if [ $? -eq 1 ]; then
  echo "Creating virtualenv for tasks"
  cd $bamboo_working_directory
  virtualenv tasks
  source $bamboo_working_directory/tasks/bin/activate
else
  echo "Using existing virtualenv tasks"
fi
cd $bamboo_working_directory/tasks
pip install -r requirements.txt
