#!/bin/bash
# install requirements for pip

source $WORKSPACE/tasks/bin/activate
if [ $? -eq 1 ]; then
  echo "Creating virtualenv for tasks"
  cd $WORKSPACE
  virtualenv tasks
  source $WORKSPACE/tasks/bin/activate
else
  echo "Using existing virtualenv tasks"
fi

cd $WORKSPACE/tasks
pip install -r requirements.txt
