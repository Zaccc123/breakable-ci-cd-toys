# Tasks

Tasks is a django app that could views tasks created by the sibling project [create_tasks](../create_tasks).

It main page: `http://localhost:8000/` can view a list of tasks.

## Solace

Note: Update ip_address on `mqtt.py` solace ipaddress.

1. Boot up solace in `VirtualBox`.

2. Login with: sysadmin qwe123qwe

Type the followling to show ip:

```bash
sysadmin@solace$ solacectl cli
solace> show ip vrf management
```

Copy the ipaddress and replace it in `mqtt.py`

## To test between tasks(this project) and create_tasks projects:
`cd` into `tasks` directory with `virtualenv` running. Do the following

```bash
$ python manage.py runserver 0.0.0.0:8000
```

Open this [link](http://localhost:8000/) with a browser. After using [create_tasks_page](http://localhost:8001/) to create a task. Refresh this [link](http://localhost:8000/) to see a new task added successfully. You should also be able to see in `runsever` console log.
