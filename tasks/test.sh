#!/bin/bash
# run test for django project (tasks)

cd tasks
source ~/.virtualenvs/tasks/bin/activate
python manage.py test
