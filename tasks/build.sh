#!/bin/bash
# install requirements for pip needed in this project (tasks)

cd tasks
source ~/.virtualenvs/tasks/bin/activate
pip install -r requirements.txt
