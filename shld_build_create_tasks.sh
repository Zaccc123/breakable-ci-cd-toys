#!/bin/bash
# check if should build create_tasks/ project

no_of_commit=$(git rev-list --count HEAD ^master)
echo "Total no of commit in branch: $no_of_commit"

if git diff --raw HEAD~$no_of_commit | grep -q create_tasks/; then
  echo "Create Tasks project changed, will start building..."
else
  exit 0
fi
