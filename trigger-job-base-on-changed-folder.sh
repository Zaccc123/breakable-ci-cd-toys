#!/bin/bash
# Trigger pipeline base on parent project folder that is modified

echo "Checking which project folder have changed"

no_of_commit=$(git rev-list --count HEAD ^master)
echo "Total no of commit in branch: $no_of_commit"

if git diff --raw HEAD~$no_of_commit | grep -q tasks/; then
  echo "Tasks project changed, triggering tasks_build_job..."
else
  exit 0
fi

if git diff --raw HEAD~$no_of_commit | grep -q create_tasks/; then
  echo "Create Tasks project changed, triggering create_tasks_build_job..."
else
  exit 0
fi
