# Breakable Django in a Monolithic Repo

This is a `breakable-toy` to test out how different CI system and its compatibility with multiple project in the same repository.

### TODO:
- [x] Create a repo with two project housed in GitLab
- [x] Manually deploy repo on beanstalk
- [x] Setup project that uses Solace (mqtt)
- [x] Add tests on individual project
- [x] Ensure that repo can ` build ` , ` test ` and ` deploy ` [beanstalk] .
- [x] Ensure that repo will only run ` build ` , and ` test ` when the main project folder is changed.

### CI Tested
- [x] GitLabCI
- [x] GoCD
- [x] Jenkins (CHOSEN)

### Futher Test with Jenkins

- [X] Each branch with specific name should trigger correct pipeline(s)
- [ ] Check changes from latest pipeline run commit to latest commit and trigger correct pipeline base on project folder changes
