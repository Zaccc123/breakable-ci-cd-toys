#!/bin/bash

jenkinsUrl="https://410b552f.ngrok.io/job"
params="buildWithParameters?token=remotetoken"


if [ "$1" == "MERGE" ]; then
  git log --full-history --no-abbrev --format=raw -M -m --raw origin/master..$GIT_COMMIT > diff.temp
  mrBranch="merge-requests/$gitlabMergeRequestIid"
  params+="&branch=$mrBranch&gitlabMergeRequestTargetProjectId=$gitlabMergeRequestTargetProjectId&gitlabMergeRequestIid=$gitlabMergeRequestIid&gitCommit=$GIT_COMMIT"
else
  git log --full-history --no-abbrev --format=raw -M -m --raw $gitlabBefore..$gitlabAfter > diff.temp
  params+="&branch=$gitlabBranch&gitCommit=$GIT_COMMIT"
fi

if grep -q "\stasks/" diff.temp; then
	echo 'changes in tasks/';
  curl "$jenkinsUrl/breakable-django-tasks-pipeline/$params"
fi

if grep -q "\screate_tasks/" diff.temp; then
	echo 'changes in create_tasks/';
  curl "$jenkinsUrl/breakable-django-createtasks-pipeline/$params"
fi
