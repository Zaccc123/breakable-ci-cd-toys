#!/bin/bash
# install requirements for pip

source $WORKSPACE/create_tasks/bin/activate
if [ $? -eq 1 ]; then
  echo "Creating virtualenv for create_tasks"
  cd $WORKSPACE
  virtualenv create_tasks
  source $WORKSPACE/create_tasks/bin/activate
else
  echo "Using existing virtualenv create_tasks"
fi
cd $WORKSPACE/create_tasks
pip install -r requirements.txt
