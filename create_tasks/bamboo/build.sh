#!/bin/bash
# install requirements for pip for bambooci

echo "Running build.sh at $bamboo_working_directory"
source $bamboo_working_directory/create_tasks/bin/activate
if [ $? -eq 1 ]; then
  echo "Creating virtualenv for create_tasks"
  cd $bamboo_working_directory
  virtualenv create_tasks
  source $bamboo_working_directory/create_tasks/bin/activate
else
  echo "Using existing virtualenv create_tasks"
fi
cd $bamboo_working_directory/create_tasks
pip install -r requirements.txt
