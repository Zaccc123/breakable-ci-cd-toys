#!/bin/bash
# install requirements for pip on create_tasks

cd create_tasks
source ~/.virtualenvs/create_tasks/bin/activate
pip install -r requirements.txt
