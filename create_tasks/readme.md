# Create Tasks Project

Create Tasks is a django app that could create task for its sibling project [tasks](../tasks).

It main page: `http://localhost:8001/` can create a tasks. Solace should be running, please follow steps below.

## Solace

Note: Update ip_address on `views.py` solace ipaddress.

Boot up solace in `VirtualBox`.

Login with: sysadmin qwe123qwe


Type the followling to show ip:
```bash
sysadmin@solace$ solacectl cli
solace> show ip vrf management
```

Copy the ipaddress and replace it in `views.py`

## To test between tasks and create_tasks(this project) projects:
`cd` into `create_tasks` directory with `virtualenv` running. Do the following

```bash
$ python manage.py runserver 0.0.0.0:8001
```


Open this [link](http://localhost:8001/) with a browser. You should see a textbox and button to create task. Visiting this [link](http://localhost:8000/) should see a new task added successfully.
