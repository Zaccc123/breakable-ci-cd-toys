from django.apps import AppConfig


class CreateTasksappConfig(AppConfig):
    name = 'create_tasksapp'
