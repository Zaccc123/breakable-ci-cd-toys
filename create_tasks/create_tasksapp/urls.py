from django.conf.urls import url
from create_tasksapp import views

urlpatterns = [
    url(r'^', views.create, name='create'),
]
