from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


# Create your tests here.
class CreateViewTests(TestCase):
    def setUp(self):
        superuser = User.objects.create_superuser('test', 'test@api.com', 'testpassword')
        self.user = superuser
        self.client.login(username=superuser.username, password='testpassword')

    def test_view_create_html(self):
        """
        Ensure that home view display all tasks
        """
        response = self.client.get(reverse('create'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Create a task!")
